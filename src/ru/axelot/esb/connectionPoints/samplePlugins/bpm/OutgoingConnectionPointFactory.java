package ru.axelot.esb.connectionPoints.samplePlugins.bpm;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import ru.axelot.esb.connectionPoints.pluginsInterfaces.IOutgoingConnectionPoint;
import ru.axelot.esb.connectionPoints.pluginsInterfaces.IOutgoingConnectionPointFactory;
import ru.axelot.esb.connectionPoints.pluginsInterfaces.IServiceLocator;

/**
 * Фабрика для создания исходящей точки подключения.
 */
public class OutgoingConnectionPointFactory implements IOutgoingConnectionPointFactory {

    private final String AUTH_SERVER = "AuthServer";
    private final String USER_NAME = "UserName";
    private final String USER_PASSWORD = "UserPassword";
    private final String VALIDITY = "Validity";
    private final String SERVICE_SETTING = "Setting";

    @Override
    public IOutgoingConnectionPoint create(Map<String, String> parameters,
                                           IServiceLocator serviceLocator) {
        if (!parameters.containsKey(AUTH_SERVER)) {
            throw new IllegalArgumentException(String.format("Не задан параметр <%s>", AUTH_SERVER));
        }
        if (!parameters.containsKey(USER_NAME)) {
            throw new IllegalArgumentException(String.format("Не задан параметр <%s>", USER_NAME));
        }
        if (!parameters.containsKey(USER_PASSWORD)) {
            throw new IllegalArgumentException(String.format("Не задан параметр <%s>", USER_PASSWORD));
        }
        if (!parameters.containsKey(SERVICE_SETTING)) {
            throw new IllegalArgumentException(String.format("Не задан параметр <%s>", SERVICE_SETTING));
        }

        Map<String, String> setting = new HashMap<>();
        try {
            Properties properties = new Properties();
            properties.load(new StringReader(parameters.get(SERVICE_SETTING)));
            for (Map.Entry pair: properties.entrySet()) {
                setting.put(pair.getKey().toString(), pair.getValue().toString());
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Ошибка при разборе параметра <%s>", SERVICE_SETTING));
        }

        String server = parameters.get(AUTH_SERVER);
        String user = parameters.get(USER_NAME);
        String password = parameters.get(USER_PASSWORD);
        int validity =  parameters.containsKey(VALIDITY)?Integer.parseInt(parameters.get(VALIDITY)):60;

        return new OutgoingConnectionPoint(server, user, password, validity, setting, serviceLocator);
    }
}
