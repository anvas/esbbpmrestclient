package ru.axelot.esb.connectionPoints.samplePlugins.bpm;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;
import ru.axelot.esb.connectionPoints.pluginsInterfaces.ISimpleOutgoingConnectionPoint;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CookieStore;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import ru.axelot.esb.connectionPoints.pluginsInterfaces.ILogger;
import ru.axelot.esb.connectionPoints.pluginsInterfaces.IMessageReplyHandler;
import ru.axelot.esb.connectionPoints.pluginsInterfaces.IServiceLocator;
import ru.axelot.esb.connectionPoints.pluginsInterfaces.Message;
import ru.axelot.esb.connectionPoints.pluginsInterfaces.MessageHandlingException;

/**
 * Исходящая точка подключения для обработки сообщений из шины данных.
 */
public class OutgoingConnectionPoint implements ISimpleOutgoingConnectionPoint {

    /**
     * Логгер.
     */
    private final ILogger _logger;

    /**
     * Директория для записи файлов.
     */
    private final String authServer;
    private final String user;
    private final String password;
    private int validity;
    private Map<String, String> setting;
    private Timer timer;
    private CookieStore httpCookieStore;
    private boolean isServiceReady;

    public OutgoingConnectionPoint(String authServer, String user, String  password,
                                   int validity, Map<String, String> setting, IServiceLocator serviceLocator) {
        if (authServer == null || authServer.equals("")) {
            throw new IllegalArgumentException("Не задан параметр <AuthServer>");
        }

        if (user == null || user.equals("")) {
            throw new IllegalArgumentException("Не задан параметр <UserName>");
        }

        if (user == null || user.equals("")) {
            throw new IllegalArgumentException("Не задан параметр <Password>");
        }

        if (setting == null || setting.isEmpty()) {
            throw new IllegalArgumentException("Не задан параметр <Setting>");
        }
        _logger = serviceLocator.getLogger(getClass());

        this.authServer = authServer;
        this.user = user;
        this.password = password;
        this.validity = validity;
        this.setting = setting;
        this.timer = new Timer();
        this.httpCookieStore = new BasicCookieStore();
        this.isServiceReady = false;
    }

    private synchronized boolean isServiceReady() {
        return isServiceReady;
    }

    private synchronized void setServiceReady(boolean serviceReady) {
        isServiceReady = serviceReady;
    }

    public synchronized CookieStore getHttpCookieStore() {
        return httpCookieStore;
    }

    public synchronized void setHttpCookieStore(CookieStore httpCookieStore) {
        this.httpCookieStore = httpCookieStore;
    }

    @Override
    public boolean isReady() {
        return isServiceReady();
    }


    @Override
    public void initialize() throws Exception {

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                _logger.debug(String.format("Выполним авторизацию по таймеру %s мс", validity));
                CredentialsProvider provider = new BasicCredentialsProvider();
                UsernamePasswordCredentials credentials
                        = new UsernamePasswordCredentials(user, password);

                provider.setCredentials(AuthScope.ANY, credentials);

                try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).
                        setDefaultCookieStore(getHttpCookieStore()).build()) {

                    JSONObject authString = new JSONObject();
                    authString.put("UserName", user);
                    authString.put("UserPassword", password);

                    _logger.debug(String.format("Авторизационная строка: %s", authString.toJSONString()));
                    _logger.debug(String.format("Сервис авторизации: %s", authServer));

                    HttpPost post = new HttpPost(authServer);
                    StringEntity postingString = new StringEntity(authString.toJSONString());
                    postingString.setContentType("application/json");
                    post.setEntity(postingString);
                    post.setHeader("Accept", "application/json");

                    long st = System.nanoTime();
                    _logger.debug("Начало выполнения авторизационного запроса");
                    HttpResponse response = httpClient.execute(post);
                    _logger.debug(String.format("Окончание выполнения авторизационного запроса. Время выполнения: %s ms", (System.nanoTime()-st)/1000000));
                    _logger.debug(String.format("Ответ сервера: %s, %s",
                            response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase()));

                    JSONObject jsonObject = (JSONObject) JSONValue.parseWithException(
                            new InputStreamReader(response.getEntity().getContent()));

                    int responseCode = Integer.parseInt(jsonObject.get("Code").toString());
                    String responseMessage = jsonObject.get("Message").toString();
                    if (responseCode==0) {
                        _logger.debug("Авторизация выполнена");
                        setServiceReady(true);
                    }else {
                        _logger.error(String.format("Не удалось выполнить авторизацию. Код: %s, по причине: %s", responseCode, responseMessage));
                        setServiceReady(false);
                    }
                }catch (IOException ex){
                    _logger.error("Не удалось выполнить авторизацию");
                    setServiceReady(false);
                } catch (ParseException e) {
                    _logger.error("Не удалось выполнить авторизацию. Сервер вернул неожиданный результат");
                    setServiceReady(false);
                }catch (Exception e){
                    _logger.error(String.format("Не удалось выполнить авторизацию. Неожиданный результат: %s", e.getMessage()));
                    setServiceReady(false);
                }

            }
        }, 100, validity);
    }

    @Override
    public boolean handleMessage(Message message, IMessageReplyHandler replyHandler) throws MessageHandlingException {

        if (!isServiceReady()){
            _logger.error("Не удалось отправить сообщение. Сервис не готов. Авторизация не выполнена");
            return false;
        }

        String serviceURL = setting.get(message.getClassId());
        if (serviceURL==null ||serviceURL.equals("")){
            _logger.error(String.format("Неизвестный класс сообщения <%s>", message.getClassId()));
            throw new MessageHandlingException("Неизвестный класс сообщения");
        }

        _logger.debug(String.format("Полученное сообщение с классом <%s>, будет передано сервису <%s> ",
                message.getClassId(), serviceURL));

        CredentialsProvider provider = new BasicCredentialsProvider();
        UsernamePasswordCredentials credentials
                = new UsernamePasswordCredentials(user, password);

        provider.setCredentials(AuthScope.ANY, credentials);

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).
                setDefaultCookieStore(getHttpCookieStore()).build()) {

            byte[] messageBody = message.getBody();

            HttpPost post = new HttpPost(serviceURL);

            String stringBody = new String(messageBody, "UTF-8");
            _logger.debug(String.format("Сообщение для отправки: %s", stringBody));

            StringEntity postingString = new StringEntity(stringBody, "UTF-8");

            postingString.setContentType("application/json");
            post.setEntity(postingString);
            post.setHeader("Accept", "application/json");

            long st = System.nanoTime();
            _logger.debug("Начало выполнения запроса к сервису");
            HttpResponse response = httpClient.execute(post);
            _logger.debug(String.format("Окончание выполнения запроса к сервису. Время выполнения: %s ms", (System.nanoTime()-st)/1000000));

            _logger.debug(String.format("Ответ сервера: %s, %s",
                    response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase()));

            JSONObject jsonObject = (JSONObject) JSONValue.parseWithException(
                    new InputStreamReader(response.getEntity().getContent()));

            Boolean isSuccess = jsonObject.containsValue("Success");
            if (isSuccess){
                _logger.debug("Сообщение успешно отправлено");
            }else {
                _logger.error("Не удалось отправить сообщение. Сервис не вернул код успеха");
                return false;
            }

        }catch (IOException ex){
            _logger.error(String.format("Не удалось отправить сообщение по причине: %s", ex.getMessage()));
            return false;
        } catch (ParseException e) {
            _logger.error(String.format("Не удалось отправить сообщение. Ошибка парсинга ответа сервиса: %s", e.toString()));
            return false;
        }catch (Exception e){
            _logger.error(String.format("Не удалось отправить сообщение. Неизвестная ошибка: %s", e.getMessage()));
            return false;
        }

        return true;
    }

    @Override
    public void cleanup() throws Exception {
        timer.cancel();
    }
}
